class StudentPostRegistration
  attr_accessor :student

  def initialize(student)
    @student = student
  end

  def post_register
    Account.new.register(student)
    StudentClub.new.register(student)
    Library.new.register(student)
  end
end

class Account
  def register(student)
    puts "Student registered in account section"
  end
end

class StudentClub
  def register(student)
    puts "Student registered in student club"
  end
end

class Library
  def register(student)
    puts "Student registerd in Library"
  end
end

StudentPostRegistration.new({name: "Alex", id: 5}).post_register

